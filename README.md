# README #

Accompanying source code for blog entry at http://tech.asimio.net/2018/02/20/Microservices-Sidecar-pattern-implementation-using-Postgres-Spring-Cloud-Netflix-and-Docker.html

### Requirements ###

* Java 8
* Maven 3.3.x
* Docker
* Eureka Server as described at https://bitbucket.org/asimio/discoveryserver/

### Building the artifact ###

```
mvn clean package
```

### See also ###

- https://bitbucket.org/asimio/postgres/overview
- https://bitbucket.org/asimio/sidecar-postgres-demo/overview
- https://bitbucket.org/asimio/discoveryserver/overview

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero